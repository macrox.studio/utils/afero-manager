package afero_manager

import "fmt"

const (
	ConfigProviderKey = "provider"
	ConfigBucketsKey  = "buckets"
)

type Config interface {
	GetProvider() string

	GetBuckets() []string

	HasBuckets() bool
	LenBuckets() int
}

type BaseConfig struct {
	// Provider name abstract fs / uri scheme
	Provider string `mapstructure:"provider" json:"provider"`

	// Buckets - raw buckets (string or array strings)
	Buckets interface{} `mapstructure:"buckets,omitempty" json:"buckets,omitempty"`
}

func (cfg BaseConfig) GetProvider() string {
	return cfg.Provider
}

func (cfg BaseConfig) HasBuckets() bool {
	return hasStrings(cfg.Buckets)
}

func (cfg BaseConfig) GetBuckets() []string {
	return getStrings(cfg.Buckets)
}

func (cfg BaseConfig) LenBuckets() int {
	return len(cfg.GetBuckets())
}

func DetectConfigProvider(i interface{}) string {
	switch v := i.(type) {
	case Config:
		return v.GetProvider()
	case map[string]interface{}:
		sub, exist := v[ConfigProviderKey]
		if !exist {
			break
		}
		return fmt.Sprint(sub)
	default:
		break
	}
	return ""
}
