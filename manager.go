package afero_manager

import (
	"fmt"
	"gitlab.com/macrox.studio/utils/afero-manager/web"
	"strings"

	"github.com/cornelk/hashmap"
	"github.com/rs/zerolog/log"
	"github.com/spf13/afero"
)

var (
	instances hashmap.HashMap
)

// Destroyed interface
type Destroyed interface {
	Destroy() error
}

// Get abstract fs by key
func Get(key ...string) (afero.Fs, error) {
	if len(key) < 1 && instances.Len() == 1 {
		for it := range instances.Iter() {
			if afs, ok := it.Value.(afero.Fs); ok && afs != nil {
				return afs, nil
			}
		}
	}
	id := strings.Join(skipEmptyStrings(key), ".")
	if len(id) < 1 {
		return nil, ErrEmptyInstanceId
	}
	i, exist := instances.Get(id)
	if !exist {
		if web.IsSupport(key[0]) && len(key) > 1 {
			return web.NewFs(key[0], key[1], nil), nil
		}
		return nil, fmt.Errorf("fs \"%s\" not found", id)
	}
	afs, ok := i.(afero.Fs)
	if !ok {
		return nil, fmt.Errorf("fs \"%s\" not supported", id)
	}
	return afs, nil
}

// Put abstract fs by key
func Put(afs afero.Fs, key ...string) afero.Fs {
	if len(key) < 1 {
		log.Warn().Msg("Fail put abstract fs, but key is empty")
		return afs
	}
	id := strings.Join(skipEmptyStrings(key), ".")
	if _, exist := instances.Get(id); exist {
		log.Warn().Str("id", id).Msg("Replace abstract fs instance")
	}
	instances.Set(id, afs)
	return afs
}

// Destroy all instances
func Destroy() (err error) {
	for it := range instances.Iter() {
		if i, ok := it.Value.(Destroyed); ok && i != nil {
			err = wrapError(err, i.Destroy())
		}
		instances.Del(it.Key)
	}
	return
}
