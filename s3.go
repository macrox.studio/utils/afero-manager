package afero_manager

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	s3 "github.com/legion-zver/afero-s3"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/afero"
)

// S3Config s3 storage options
type S3Config struct {
	BaseConfig `mapstructure:",squash"`

	// Name additional key for get abstract fs instance
	Name string `mapstructure:"name,omitempty" json:"name,omitempty"`

	// Default bucket
	DefaultBucket string `mapstructure:"default_bucket,omitempty" json:"default_bucket,omitempty"`

	// S3 Options
	Region         string      `mapstructure:"region,omitempty" json:"region,omitempty" json:"region,omitempty"`
	Endpoint       string      `mapstructure:"endpoint,omitempty" json:"endpoint,omitempty" json:"endpoint,omitempty"`
	DisableSSL     interface{} `mapstructure:"disable_ssl,omitempty" json:"disable_ssl,omitempty"`
	ForcePathStyle interface{} `mapstructure:"force_path_style,omitempty" json:"force_path_style,omitempty"`

	// S3 Credentials
	KeyID           string `mapstructure:"key_id" json:"key_id" json:"key_id,omitempty"`
	SecretAccessKey string `mapstructure:"secret_access_key" json:"secret_access_key" json:"secret_access_key,omitempty"`
}

func (cfg S3Config) IsDisableSSL() bool {
	return getBool(cfg.DisableSSL)
}

func (cfg S3Config) IsForcePathStyle() bool {
	return getBool(cfg.ForcePathStyle)
}

func PutS3FsByConfig(i interface{}) (afero.Fs, error) {
	var cfg S3Config
	if err := mapstructure.Decode(i, &cfg); err != nil {
		return nil, err
	}
	if !cfg.HasBuckets() {
		return nil, ErrEmptyBuckets
	}
	if len(cfg.Region) < 1 {
		return nil, ErrEmptyRegion
	}
	if len(cfg.Provider) < 1 {
		cfg.Provider = "s3"
	}
	if len(cfg.DefaultBucket) < 1 {
		buckets := cfg.GetBuckets()
		if len(buckets) < 1 {
			return nil, ErrEmptyBuckets
		}
		cfg.DefaultBucket = buckets[0]
	}
	sess, err := session.NewSession(&aws.Config{
		Region:           aws.String(cfg.Region),
		Endpoint:         aws.String(cfg.Endpoint),
		DisableSSL:       aws.Bool(cfg.IsDisableSSL()),
		S3ForcePathStyle: aws.Bool(cfg.IsForcePathStyle()),
		Credentials:      credentials.NewStaticCredentials(cfg.KeyID, cfg.SecretAccessKey, ""),
	})
	if err != nil {
		return nil, err
	}
	afs := s3.NewFs(cfg.DefaultBucket, sess)
	Put(afs, cfg.Provider, cfg.Name, cfg.DefaultBucket)
	if buckets := cfg.GetBuckets(); len(buckets) > 0 {
		for _, bucket := range buckets {
			if bucket == cfg.DefaultBucket {
				continue
			}
			Put(s3.NewFs(bucket, sess), cfg.Provider, cfg.Name, bucket)
		}
	} else {
		Put(afs, cfg.Provider, cfg.Name)
	}
	return afs, nil
}
