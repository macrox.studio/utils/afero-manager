package web

import (
	"fmt"
	"github.com/spf13/afero"
	"io"
	"io/ioutil"
	"net/http"
)

func IsSupport(provider string) bool {
	return provider == "http" || provider == "https"
}

func respToError(resp *http.Response) error {
	if resp == nil {
		return nil
	}
	if resp.StatusCode%100 == 2 {
		return nil
	}
	if resp.StatusCode == 404 {
		return afero.ErrFileNotFound
	}
	if resp.Body == nil {
		return fmt.Errorf("reponse status code = %d", resp.StatusCode)
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(resp.Body)
	body, _ := ioutil.ReadAll(resp.Body)
	if body == nil {
		body = []byte("unknown")
	}
	return fmt.Errorf("reponse status code = %d, reason %s", resp.StatusCode, string(body))
}
