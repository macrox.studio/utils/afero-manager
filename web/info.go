package web

import (
	"io/fs"
	"net/http"
	"time"
)

type Info struct {
	resp *http.Response
	name string
}

func (i Info) Url() string {
	return i.resp.Request.URL.String()
}

func (i Info) Name() string {
	return i.name
}

func (i Info) Size() int64 {
	return i.resp.ContentLength
}

func (i Info) Mode() fs.FileMode {
	return 0
}

func (i Info) ModTime() time.Time {
	v := i.resp.Header.Get("Last-Modified")
	if len(v) < 1 {
		return time.Time{}
	}
	lastModified, _ := time.Parse(time.RFC1123, v)
	return lastModified
}

func (i Info) IsDir() bool {
	return false
}

func (i Info) Sys() interface{} {
	return nil
}
