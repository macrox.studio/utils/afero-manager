package web

import (
	"context"
	"fmt"
	"github.com/cornelk/hashmap"
	"github.com/spf13/afero"
	"io"
	"net/http"
	"os"
	"time"
)

type File struct {
	cancels hashmap.HashMap
	name    string
	info    os.FileInfo
	off     int64
	fs      *Fs
}

func (f *File) cancelRequests() {
	defer func() {
		_ = recover()
	}()
	for it := range f.cancels.Iter() {
		if it.Value != nil {
			if fn, ok := it.Value.(context.CancelFunc); ok && fn != nil {
				fn()
			}
		}
		f.cancels.Del(it.Key)
	}
}

func (f *File) Close() error {
	f.cancelRequests()
	f.info = nil
	f.fs = nil
	return nil
}

func (f *File) Read(p []byte) (n int, err error) {
	n, err = f.ReadAt(p, f.off)
	if err != nil {
		return
	}
	f.off += int64(n)
	return
}

func (f *File) ReadAt(p []byte, off int64) (n int, err error) {
	if f.fs == nil {
		return 0, afero.ErrFileClosed
	}
	req, err := http.NewRequest(http.MethodGet, f.fs.GetUrl(f.Name()), nil)
	if err != nil {
		return 0, err
	}
	id := time.Now().Nanosecond()
	ctx, cancel := context.WithCancel(context.Background())
	req = req.WithContext(ctx)
	req.Header.Add("X-Request-ID", fmt.Sprint(id))
	req.Header.Add("Range", fmt.Sprintf("bytes=%d-%d", off, len(p)))
	f.cancels.Set(id, cancel)
	defer func() {
		f.cancels.Del(id)
	}()
	resp, err := f.fs.client.Do(req)
	if err != nil {
		return 0, err
	}
	err = respToError(resp)
	if err != nil {
		return 0, err
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(resp.Body)
	return resp.Body.Read(p)
}

func (f *File) Seek(offset int64, whence int) (int64, error) {
	switch whence {
	case io.SeekStart:
		f.off = offset
		break
	case io.SeekCurrent:
		f.off = f.off + offset
		break
	case io.SeekEnd:
		info, err := f.Stat()
		if err != nil {
			return f.off, err
		}
		f.off = info.Size() - offset
		if f.off < 0 {
			f.off = 0
		}
		break
	}
	return f.off, nil
}

func (f *File) Write(p []byte) (n int, err error) {
	return 0, ErrOperationNotSupported
}

func (f *File) WriteAt(p []byte, off int64) (n int, err error) {
	return 0, ErrOperationNotSupported
}

func (f *File) Name() string {
	return f.name
}

func (f *File) Readdir(count int) ([]os.FileInfo, error) {
	return []os.FileInfo{}, ErrOperationNotSupported
}

func (f *File) Readdirnames(n int) ([]string, error) {
	return []string{}, ErrOperationNotSupported
}

func (f *File) Stat() (os.FileInfo, error) {
	if f.info == nil {
		return nil, afero.ErrFileClosed
	}
	return f.info, nil
}

func (f *File) Sync() error {
	return nil
}

func (f *File) Truncate(size int64) error {
	return ErrOperationNotSupported
}

func (f *File) WriteString(s string) (ret int, err error) {
	return 0, ErrOperationNotSupported
}
