package web

import "errors"

var (
	ErrOperationNotSupported = errors.New("operation not supported")
)
