package web

import (
	"fmt"
	"github.com/spf13/afero"
	"net/http"
	"os"
	"strings"
	"time"
)

type Fs struct {
	client *http.Client
	schema string
	host   string
}

func (fs *Fs) GetSchema() string {
	return fs.schema
}

func (fs *Fs) GetHost() string {
	return fs.host
}

func (fs *Fs) GetUrl(name string) string {
	return fmt.Sprintf(
		"%s://%s/%s", fs.GetSchema(),
		strings.TrimSuffix(fs.GetHost(), "/"),
		strings.TrimPrefix(strings.TrimSpace(name), "/"),
	)
}

func (fs *Fs) Create(name string) (afero.File, error) {
	return nil, ErrOperationNotSupported
}

func (fs *Fs) Mkdir(name string, perm os.FileMode) error {
	return ErrOperationNotSupported
}

func (fs *Fs) MkdirAll(path string, perm os.FileMode) error {
	return ErrOperationNotSupported
}

func (fs *Fs) Open(name string) (afero.File, error) {
	info, err := fs.Stat(name)
	if err != nil {
		return nil, err
	}
	return &File{name: name, info: info, fs: fs}, nil
}

func (fs *Fs) OpenFile(name string, flag int, perm os.FileMode) (afero.File, error) {
	if flag != os.O_RDONLY {
		return nil, ErrOperationNotSupported
	}
	return fs.Open(name)
}

func (fs *Fs) Remove(name string) error {
	req, err := http.NewRequest(http.MethodDelete, fs.GetUrl(name), nil)
	if err != nil {
		return err
	}
	resp, err := fs.client.Do(req)
	if err != nil {
		return err
	}
	return respToError(resp)
}

func (fs *Fs) RemoveAll(path string) error {
	return fs.Remove(path)
}

func (fs *Fs) Stat(name string) (os.FileInfo, error) {
	resp, err := fs.client.Head(fs.GetUrl(name))
	if err != nil {
		return nil, err
	}
	if err = respToError(resp); err != nil {
		return nil, err
	}
	return &Info{resp: resp, name: name}, nil
}

func (fs *Fs) Rename(oldname, newname string) error {
	return ErrOperationNotSupported
}

func (Fs) Name() string {
	return "WebFs"
}

func (fs *Fs) Chmod(name string, mode os.FileMode) error {
	return ErrOperationNotSupported
}

func (fs *Fs) Chown(name string, uid, gid int) error {
	return ErrOperationNotSupported
}

func (fs *Fs) Chtimes(name string, atime time.Time, mtime time.Time) error {
	return ErrOperationNotSupported
}

func NewFs(schema, host string, client *http.Client) afero.Fs {
	if client == nil {
		client = &http.Client{}
	}
	return &Fs{
		client: client,
		schema: schema,
		host:   host,
	}
}
