module gitlab.com/macrox.studio/utils/afero-manager

go 1.17

require (
	github.com/aws/aws-sdk-go v1.42.13
	github.com/cornelk/hashmap v1.0.1
	github.com/legion-zver/afero-s3 v0.4.17
	github.com/mitchellh/mapstructure v1.4.2
	github.com/pkg/errors v0.9.1
	github.com/rs/zerolog v1.26.0
	github.com/spf13/afero v1.6.0
)

require (
	github.com/dchest/siphash v1.1.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	golang.org/x/text v0.3.7 // indirect
)
