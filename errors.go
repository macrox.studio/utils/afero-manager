package afero_manager

import "errors"

var (
	ErrEmptyInstanceId = errors.New("empty fs instance id")
	ErrEmptyBuckets    = errors.New("buckets is empty")
	ErrEmptyRegion     = errors.New("region is empty")
	ErrWrongConfig     = errors.New("wrong config")
)
