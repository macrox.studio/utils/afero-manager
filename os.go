package afero_manager

import (
	"path"
	"strings"

	"github.com/mitchellh/mapstructure"
	"github.com/spf13/afero"
)

type OsConfig struct {
	BaseConfig `mapstructure:",squash"`

	// Path - base path
	Path string `mapstructure:"path,omitempty" json:"path,omitempty"`
}

func ParseOsConfig(i interface{}) (*OsConfig, error) {
	switch v := i.(type) {
	case OsConfig:
		return &v, nil
	case *OsConfig:
		return v, nil
	case map[string]interface{}:
		var cfg OsConfig
		if err := mapstructure.Decode(i, &cfg); err != nil {
			return nil, err
		}
		return &cfg, nil
	default:
		return nil, ErrWrongConfig
	}
}

func PutOsFsByConfig(i interface{}) (afero.Fs, error) {
	cfg, err := ParseOsConfig(i)
	if err != nil {
		return nil, err
	}
	if len(cfg.Provider) < 1 {
		cfg.Provider = "fs"
	}
	afs := afero.NewOsFs()
	if len(cfg.Path) > 1 {
		if !strings.HasPrefix(cfg.Path, "/") {
			cfg.Path = path.Join("/", cfg.Path)
		}
		afs = afero.NewBasePathFs(afs, cfg.Path)
	}
	if buckets := cfg.GetBuckets(); len(buckets) > 0 {
		for _, bucket := range buckets {
			Put(afs, cfg.Provider, bucket)
		}
	} else {
		Put(afs, cfg.Provider)
	}
	return afs, err
}
