package afero_manager

import (
	"strings"

	"github.com/pkg/errors"
)

func trimSpaceStrings(input []string) []string {
	for i := range input {
		input[i] = strings.TrimSpace(input[i])
	}
	return input
}

func skipEmptyStrings(input []string) []string {
	filtered := make([]string, 0)
	for _, v := range input {
		if len(strings.TrimSpace(v)) > 0 {
			filtered = append(filtered, v)
		}
	}
	return filtered
}

func hasStrings(i interface{}) bool {
	if i == nil {
		return false
	}
	switch v := i.(type) {
	case string:
		return len(v) > 0
	case []string:
		return len(v) > 0
	default:
		return false
	}
}

func getStrings(i interface{}) []string {
	if i == nil {
		return []string{}
	}
	switch v := i.(type) {
	case string:
		return trimSpaceStrings(strings.Split(v, ","))
	case []string:
		return trimSpaceStrings(v)
	default:
		return []string{}
	}
}

func getBool(i interface{}) bool {
	if i != nil {
		switch v := i.(type) {
		case bool:
			return v
		case byte:
			return v > 0
		case int:
			return v > 0
		case int64:
			return v > 0
		case string:
			v = strings.ToLower(v)
			return v == "true" || v == "on" || v == "yes" || v == "y" || v == "1"
		default:
			break
		}
	}
	return false
}

func wrapError(first, second error) error {
	if first == nil {
		if second != nil {
			return second
		}
	}
	if second != nil {
		return errors.Wrap(first, second.Error())
	}
	return first
}
